package TenIsland;

import java.util.Scanner;

public class Player {
    private int positionX;
    private int positionY;

    private int count;
    private Scanner myScan = new Scanner(System.in);

    public final Character[] islandY = new Character[]{'A', 'B', 'C','D','E','F','G','H','I','J'};
    public final int[] islandX = new int[]{1,2,3,4,5,6,7,8,9,10};

    public final int lengthY = islandY.length-1;
    public final int lengthX = islandX.length-1;

    void currentPosition(){
        System.out.println("Your position: "+islandY[positionY]+islandX[positionX]);
    }

    public Player(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }


    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }



    public void move(Character direction, int count){

        switch (direction){
            case 'N':
                if(positionY==0){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionY -= count;
                }
                break;
            case 'S':
                if(positionY==lengthY){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionY += count;
                }
                break;
            case 'W':
                if(positionX==0){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionX -= count;
                }
                break;
            case 'E':
                if(positionX==lengthX){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionX += count;
                }
                break;
        }
    }

    public void pullOver(Character direction){

        switch (direction){
            case 'N':
                if(positionY==0){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionY = 0;
                }
                break;
            case 'S':
                if(positionY==lengthY){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionY = lengthY;
                }
                break;
            case 'W':
                if(positionX==0){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionX = 0;
                }
                break;
            case 'E':
                if(positionX==lengthX){
                    System.out.println("Sorry, you're in edge!");
                }else{
                    positionX = lengthX;
                }
                break;
        }
    }

    public void reset(){
        positionX = 4;
        positionY =5;
    }

}
