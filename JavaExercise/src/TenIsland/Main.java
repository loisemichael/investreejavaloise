package TenIsland;

import java.util.Scanner;


public class Main {



    public static void main(String[] args) {
        Player p1 = new Player(4,5);
        p1.currentPosition();
        int myOption,step;
        char myDirection;
        Scanner myScan = new Scanner(System.in);

        do {
            option();
            myOption = myScan.nextInt();
            switch (myOption){
                case 1: // Move
                    direction();
                    myDirection = myScan.next().toUpperCase().charAt(0);
                    System.out.print("Number of steps: ");
                    step = myScan.nextInt();
                    p1.move(myDirection,step);
                    p1.currentPosition();
                    break;
                case 2: // pull Over
                    direction();
                    myDirection = myScan.next().toUpperCase().charAt(0);
                    p1.pullOver(myDirection);
                    p1.currentPosition();
                    break;
                case 3:
                    p1.reset();
                    p1.currentPosition();
                    break;
                case 4:
                    System.out.println("Thank you for using my Apps");
                    break;
            }
        }while (myOption != 4);
    }



    public static void direction(){
        System.out.println("Choose direction : ");
        System.out.println("N -> North");
        System.out.println("S -> South");
        System.out.println("W -> West");
        System.out.println("E -> East");
        System.out.print("Choose: ");
    }

    public static void option(){
        System.out.println("Choose one: ");
        System.out.println("1. Move - change your position with 1 step");
        System.out.println("2. Pull Over - change your position to edge");
        System.out.println("3. Reset - reset your position to F5");
        System.out.println("4. Exit");
        System.out.print("Your Choose: ");
    }



}
