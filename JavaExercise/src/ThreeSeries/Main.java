package ThreeSeries;

import java.util.Scanner;

public class Main {

    public static final int[] myArr = new int[]{1, 2, 3, 3, 9, 10, 12, 34, 39, 46, 131, 150, 177};

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        int input;
        System.out.print("Masukkan angka dari 0 sampai "+(myArr.length-1)+": ");
        input = myScanner.nextInt();
        print(input);

    }

    public static void print(int myInput){
        System.out.println("Angka Pertama: "+myArr[myInput]);
        if(myInput == (myArr.length-1)) System.out.println("Angka Pertama: "+myArr[myInput-1]);
        else  System.out.println("Angka Pertama: "+myArr[myInput+1]);
        System.out.println("Angka Ketiga: "+myArr[0]);
    }

}
