package BentukRuang;

import java.util.Scanner;

public class Segitiga implements BentukRuang{
    private double alas,tinggi;
    private Scanner myScan = new Scanner(System.in);

    Segitiga(double alas, double tinggi){
        this.alas = alas;
        this.tinggi = tinggi;
    }

    Segitiga(){}


    @Override
    public void count() {
        System.out.println("Luas Segitiga: "+(alas*tinggi/2)+"\n");
    }

    @Override
    public void input() {
        System.out.print("Alas: ");
        alas = myScan.nextDouble();
        System.out.print("Tinggi: ");
        tinggi = myScan.nextDouble();
        count();

    }


}
