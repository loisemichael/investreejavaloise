package BentukRuang;

import java.util.Scanner;

public class Kubus implements BentukRuang{
    private double sisi;
    private Scanner myScan = new Scanner(System.in);

    Kubus(double sisi){
        this.sisi = sisi;
    }

    Kubus(){}


    @Override
    public void count() {
        double volume = Math.pow(sisi,3);
        System.out.println("Volume Kubus: "+volume+"\n");
    }

    @Override
    public void input() {
        System.out.print("Sisi: ");
        sisi = myScan.nextDouble();
        count();

    }
}
