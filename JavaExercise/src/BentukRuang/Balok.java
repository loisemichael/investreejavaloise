package BentukRuang;

import java.util.Scanner;

public class Balok implements BentukRuang{

    private double panjang,lebar,tinggi;
    private Scanner myScan = new Scanner(System.in);

    Balok(double panjang,double lebar, double tinggi){
        this.panjang = panjang;
        this.lebar = lebar;
        this.tinggi = tinggi;
    }

    Balok(){}


    @Override
    public void count() {
        double volume = panjang*lebar*tinggi;
        System.out.println("Volume Balok: "+volume+"\n");
    }

    @Override
    public void input() {
        System.out.print("Panjang: ");
        panjang = myScan.nextDouble();
        System.out.print("Lebar: ");
        lebar = myScan.nextDouble();
        System.out.print("Tinggi: ");
        tinggi = myScan.nextDouble();
        count();
    }
}
