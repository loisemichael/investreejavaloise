package BentukRuang;

import java.util.Scanner;

public class Main {
    public static Scanner myScan = new Scanner(System.in);
    public static int option;
    public static void main(String[] args) {
        Segitiga s1 = new Segitiga();
        Balok b1 = new Balok();
        Kubus k1 = new Kubus();

        do {
            optionMenu();
            switch (option){
                case 1:
                    s1.input();
                    break;
                case 2:
                    b1.input();
                    break;
                case 3:
                    k1.input();
                    break;
            }
        }while (option!=4);
    }

    static void optionMenu(){
        System.out.println("Kalkulator Bangun");
        System.out.println("1. Segitiga");
        System.out.println("2. Balok");
        System.out.println("3. Kubus");
        System.out.println("4. Exit");
        System.out.print("Menu: ");
        option = myScan.nextInt();
    }
}
