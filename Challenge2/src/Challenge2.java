import java.util.Scanner;

public class Challenge2 {
    public static void main(String[] args) {
        int currentFloor;
        int loop;
        int inputFloor=1;
        currentFloor = 1;


        Scanner myScanner = new Scanner(System.in);

        while (currentFloor>0 && currentFloor <=30 && inputFloor>0 && inputFloor<31){
            System.out.print("Enter floor: ");

            inputFloor = myScanner.nextInt();

            if(currentFloor<=inputFloor && inputFloor>0 && inputFloor<31){
                for(loop = currentFloor;loop<inputFloor;loop++){
                    System.out.println("Elevator at floor "+loop);
                }
                currentFloor = inputFloor;
                System.out.println("Elevator stop at floor "+currentFloor);
            }else if(currentFloor>=inputFloor && inputFloor>0 && inputFloor<31){
                for(loop = currentFloor;loop>=inputFloor;loop--){
                    System.out.println("Elevator at floor "+loop);
                }
                currentFloor = inputFloor;
                System.out.println("Elevator stop at floor "+currentFloor);
            }

        }

    }

}
