import java.util.ArrayList;

public class MaxNumber {
    public static void main(String[] args) {
        {
            int arrayNum[] = {1, 2, 3, 3, 4, 4, 5, 6,7,7};
            int maxNum = 0;
            int tempNum = 0;
            boolean duplicate = false;

            for (int x = 0; x < arrayNum.length; x++) {
                for (int z = 0; z < arrayNum.length; z++) {
                    if (x == z) continue;
                    if (arrayNum[x] == arrayNum[z]){
                        duplicate = true;
                        break;
                    }
                    else if (arrayNum[x] != arrayNum[z]){
                        duplicate = false;
                        if (arrayNum[x]>arrayNum[z] && arrayNum[x]>maxNum){
                            tempNum = arrayNum[x];
                        }
                        else break;
                    }
                }
                if(duplicate == false){
                    maxNum = tempNum;
                }
            }

            System.out.println(maxNum);
        }


    }
}
