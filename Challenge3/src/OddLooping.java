import java.util.ArrayList;

public class OddLooping
{
    public static void main(String[] args) {
        int y=10;
        int x;
        boolean myBoolean;
        ArrayList<Integer> oddNumber = new ArrayList<Integer>();
        ArrayList<Integer> evenNumber = new ArrayList<Integer>();

        for(x=1;x<=y;x++){
            myBoolean = checkNumber(x);
            if(myBoolean == true){
                oddNumber.add(x);
            }
            if(myBoolean == false){
                evenNumber.add(x);
            }
        }

        // Ganjil
        System.out.println("Angka Ganjil");
        for (int i = 0; i < oddNumber.size(); i++) {
            System.out.println(oddNumber.get(i));
        }

        // Genap
        System.out.println("Angka Genap");
        for (int i = 0; i < evenNumber.size(); i++) {
            System.out.println(evenNumber.get(i));
        }
    }

    static boolean checkNumber(int number) {
        int myNumber;
        if(number % 2 != 0){
            return true; // Ganjil = true
        }else{
            return false; // Genap = false
        }


    }
}

